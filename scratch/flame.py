import numpy as np
import numba as nb


kwargs = {
    "cache": True,
    "nopython": True,
    "nogil": True,
}


@nb.jit(**kwargs)
def F0(x, y):
    return x/2, y/2


@nb.jit(**kwargs)
def F1(x, y):
    return x/2 + 1/2, y/2


@nb.jit(**kwargs)
def F2(x, y):
    return x/2, y/2 + 1/2


@nb.jit(**kwargs)
def get_new_point(x, y):
    i = np.random.randint(3)
    colors = np.array([
        (255, 0, 0),
        (0, 255, 0),
        (0, 0, 255)
    ])

    if i == 0:
        new_x, new_y = x/2, y/2
    if i == 1:
        new_x, new_y = x/2 + 1/2, y/2
    if i == 2:
        new_x, new_y = x/2, y/2 + 1/2 
    c = colors[i]
    return new_x, new_y, c


@nb.jit(cache=True, nogil=True)
def make_histogram(F, fhist, chist, num_points, maxiter):
    # TODO: numba cfunc might work
    x_edge = np.linspace(0, 1, fhist.shape[0])
    y_edge = np.linspace(0, 1, fhist.shape[1])
    for n in range(num_points):
        x, y = np.random.uniform(0, 1, size=2)
        for i in range(maxiter):
            x, y, c = F(x, y)
            # x, y, c = get_new_point(x, y)
            if i < 20:
                continue
            xidx = np.searchsorted(x_edge, x)
            yidx = np.searchsorted(y_edge, y)
            fhist[xidx, yidx] += 1
            chist[xidx, yidx] += c
            chist[xidx, yidx] /= 2


#@nb.stencil
def average(A):
    return (
        A[0, -1] + A[0, 0] + A[0, 1] +
        A[1, -1] + A[1, 0] + A[1, 1] + 
        A[2, -1] + A[2, 0] + A[2, 1]
    )/9.0


@nb.jit(cache=True, nopython=True, nogil=True)
def epanechnikov(x):
    if abs(x) >= 1:
        return 0
    return 2/np.pi*(1 - x*x)


@nb.jit(cache=True, nopython=True, nogil=True)
def width_estimation(f, N, alpha, c=1):
    return c*(1/N)**alpha*(1/max(f, 0.1))**(0.5)


@nb.jit(cache=True, nopython=True, nogil=True)
def kde(t, image):
    """h: number of bins"""
    acc = 0
    h = width_estimation(t, image.size, 0.2)
    for i in range(image.shape[0]):
        for j in range(image.shape[1]):
            acc += 1/(N*h**2)*epanechnikov((t - image[i][j])/h)
    return acc


@nb.jit(cache=True, nopython=True, nogil=True)
def apply_kernel(image):
    new_image = image.copy()
    for i in range(image.shape[0]):
        for j in range(image.shape[1]):
            new_image[i][j] = kde(image[i][j], image)
    return new_image


@nb.jit(**kwargs)
def gamma(fhist, chist):
    fmax = fhist.max()
    for i in range(fhist.shape[0]):
        for j in range(fhist.shape[1]):
            if fhist[i, j] > 0:
                chist[i, j] *= (np.log(fhist[i, j])/np.log(fmax))**(1/2.2)


if __name__ == "__main__":
    import time
    N = 100
    fhist = np.zeros(shape=(N, N))
    chist = np.zeros(shape=(N, N, 3))

    tick = time.clock()
    make_histogram(get_new_point, fhist, chist, num_points=1000, maxiter=30)
    # fhist = apply_kernel(fhist)
    #chist = apply_kernel(chist)


    # ### Apply Kernel
    # # fhist, chist = average(fhist, chist, 3)
    # fhist_avg = average(fhist)
    # chist_avg = np.zeros(shape=(*fhist.shape, 3))
    # chist[:, :, 0] = average(chist[:, :, 0])
    # chist[:, :, 1] = average(chist[:, :, 1])
    # chist[:, :, 2] = average(chist[:, :, 2])

    gamma(fhist, chist)
    # print(time.clock() - tick)
    # np.save("fhist.npy", fhist)
    # np.save("chist.npy", chist)

    import matplotlib as mpl
    mpl.use("Agg")

    import matplotlib.pyplot as plt
    fig = plt.figure()
    ax = fig.add_subplot(111)
    ax.imshow(chist)
    fig.savefig("bar.png")
