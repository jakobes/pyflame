from math import sin, cos, pi, sqrt, exp, cosh, sinh

import numpy as np
import numba as nb


def affine_colour_transform(x, y):
    i = np.random.randint(3)
    colour_array = np.array([
        (155, 0, 0),
        (0, 155, 0),
        (0, 0, 155),
    ])
    if i == 0:
        x, y = (1 + x)/2, (1 + y)/2
    elif i == 1:
        x, y = (1 + x)/2, y/2
    elif i == 2:
        x, y = x/2, (1 + y)/2

    colour = colour_array[i]
    return x, y, colour


def make_histogram(affine_colour_transform, fhist, chist, num_points, maxiter, cutoff=1):
    inner_affine_colour_transform = nb.jit(cache=True, nopython=True, nogil=True)(
        affine_colour_transform
    )

    @nb.jit(cache=True, nopython=True, nogil=True)
    def inner_loop(fhist, chist):
        x_edge = np.linspace(0, 1, fhist.shape[0])
        y_edge = np.linspace(0, 1, fhist.shape[1])

        for n in range(num_points):
            x, y = np.random.uniform(0, 1, size=2)

            for i in range(maxiter):
                x, y, colour = inner_affine_colour_transform(x, y)

                if i < cutoff:
                    continue

                xidx = np.searchsorted(x_edge, x)
                yidx = np.searchsorted(y_edge, y)
                fhist[xidx, yidx] += 1
                chist[xidx, yidx] += colour
                chist[xidx, yidx] /= 2
    inner_loop(fhist, chist)


@nb.jit(cache=True, nopython=True, nogil=True)
def gamma(fhist, chist):
    fmax = fhist.max()
    for i in range(fhist.shape[0]):
        for j in range(fhist.shape[1]):
            if fhist[i, j] > 0:
                chist[i, j] *= (np.log(fhist[i, j])/np.log(fmax))**(1/2.2)


@nb.stencil
def average(x):
    kernel = (
        x[-1, -1] + x[-1, 0] + x[-1, 1] +
        x[0, -1] + x[0, 0] + x[0, 1] +
        x[1, -1] + x[1, 0] + x[1, 1]
    )
    return kernel/9


if __name__ == "__main__":
    import time
    N = 3000
    fhist = np.zeros(shape=(N, N))
    chist = np.zeros(shape=(N, N, 3))

    tick = time.clock()
    make_histogram(affine_colour_transform, fhist, chist, num_points=100000, maxiter=30, cutoff=15)
    tock = time.clock()
    print("histogram finished: ", tock - tick)

    tick = time.clock()
    average(fhist, out=fhist)
    for c in range(3):
        average(chist[:, :, c], out=chist[:, :, c])
    tock = time.clock()
    print("stencil finished: ", tock - tick)
    tick = time.clock()
    gamma(fhist, chist)
    tock = time.clock()
    print("gamma finished: ", tock - tick)

    np.save("chist.npy", chist)

    from PIL import Image
    img = Image.fromarray(chist.astype(np.uint8))
    img.save("bar.png")
