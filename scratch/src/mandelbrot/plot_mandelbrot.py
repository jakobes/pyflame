"""Plot the Mandelbrot set."""

import numpy as np

import matplotlib as mpl
import matplotlib.pyplot as plt

from PIL import Image


def plot_image(img, cmap, light, filename):
    fig = plt.figure(figsize=(10, 10))
    ax = fig.add_axes([0.0, 0.0, 1.0, 1.0], frameon=False, aspect=1)
    norm = mpl.colors.LogNorm()     # vmin, vmax, clip

    print(img.max())
    print(img.min())

    if light:
        light = mpl.colors.LightSource(
            azdeg=115,             # 315
            altdeg=10,             # 45
            hsv_min_val=0.0,       # 0
            hsv_max_val=1.0,       # 1
            hsv_min_sat=1.0,       # 1
            hsv_max_sat=0.0        # 0
        )
        M = light.shade(
            img.T,
            cmap=plt.get_cmap(cmap),
            norm=mpl.colors.LogNorm(),
            blend_mode="hsv",      # hsv, blend_mode, soft
            vmin=None,
            vmax=None,
            vert_exag=1.5,
            # fraction=0.0
        )
    else:
        mapper = plt.cm.ScalarMappable(norm=norm, cmap=cmap)
        M = mapper.to_rgba(img.T)

    ax.imshow(M, interpolation="bicubic")
    ax.set_xticks([])
    ax.set_yticks([])
    plt.savefig(filename)
    # image = Image.fromarray(np.uint8(mapper.to_rgba(img.T)*255))
    # image = Image.fromarray(img, mode="RGBA")
    # image.save(filename)
