"""Variable width kernel density estimator."""

from math import pi

import numpy as np
import numba as nb

from numba.types import float64 as nbfloat64
from numba.types import int64 as nbint64
from numba.types import void as nbvoid


NB_KWARGS = {
    "cache": True,
    "nopython": True,
    "nogil": True,
    "fastmath": True
}


@nb.jit(nbfloat64(nbfloat64), **NB_KWARGS)
def epanechnikov(u):
    """The Kernel expects the input histogram has values in [0, 1]."""
    if abs(u) > 1:
        return 0
    return 2/pi*(1 - u*u)


@nb.jit(nb.types.UniTuple(nbint64, 2)(nbint64, nbint64, nbint64), **NB_KWARGS)
def get_range(current_index, perspecuity, max_index):
    """Return an interval centered ad `current_index` of width `perspecuty`.

    Given an array of length `max_index`, return an interval defined by
    (`start_index`, `stop_index`) of with `perspecuity` centered at `current_index`. The
    interval is truncated if `current_index` is closer to the bounds of the array than
    `perspecuity` / 2.

    This function is intended to limit a KDE to only look at neighbouring cells.
    """
    start_index = max(0, current_index - perspecuity//2)
    stop_index = min(max_index, current_index + perspecuity//2)
    return start_index, stop_index


@nb.jit(nbfloat64(nbint64, nbint64, nbint64, nbfloat64[:, :], nbfloat64), **NB_KWARGS)
def fhat(input_i, input_j, perspecuity, image, bandwidth):
    """Compute fhat using an epanechnikov kernel, and a perspecuity parameter."""
    f_accumulator = 0
    for ii in range(*get_range(input_i, perspecuity, image.shape[0])):
        for jj in range(*get_range(input_j, perspecuity, image.shape[1])):
            # if img[i, j] == 0:
            #     continue
            f_accumulator += epanechnikov((image[input_i, input_j] - image[ii, jj])/bandwidth)
    return f_accumulator/(perspecuity**2)


@nb.jit(nbfloat64(nbint64, nbint64, nbint64, nbfloat64[:, :], nbfloat64), **NB_KWARGS)
def variable__width_fhat(input_i, input_j, perspecuity, image, fixed_bandwidth):
    """Compute a variable width fhat using an epanechnikov kernel, and a perspecuity parameter.

    The implementation is inspired by Suykens and Willems.
    """
    C = 1           # Scaling parameter for local bandwidth
    alpha = 1       # Exponential scaling factor for local bandwidth

    f_accumulator = 0
    for ii in range(*get_range(input_i, perspecuity, img.shape[0])):
        for jj in range(*get_range(input_j, perspecuity, img.shape[1])):
            F = image[ii, jj]     # TODO: No more magic?
            P = max(1, fhat(ii, jj, perspecuity, image, fixed_bandwidth))
            local_bandwidth = C*sqrt(1/P)*(1/N)**alpha
            f_accumulator += F/(P*fixed_bandwidth**2)*epanachikovish(
                (image[input_i, input_j] - image[ii, jj])/local_bandwidth
            )
    return f_accumulator


@nb.jit(nbfloat64[:, :](nbfloat64[:, :], nbint64), **NB_KWARGS)
def variabel_width_kde(image, perspecuity):
    """Compute a variable with KDE."""
    out_image = image.copy()      # Copy image, so we do not overwrite during computation of fhat
    fixed_bandwidth = (1/image.size)**(-1/5)

    for i in range(image.shape[0]):
        for j in range(image.shape[0]):
            out_image[i, j] = variable_fhat(i, j, perspecuity, image, fixed_bandwidth)
    return out_image


@nb.jit(nbvoid(nbfloat64[:, :], nbint64, nbint64), **NB_KWARGSs)
def kde_iteration(image, perspecuity, maximum_iterations):
    """Compute a succession of variable width KDEs."""
    for i in range(maximum_iterations):
        image[:] = (maximum_iterations - i)/i*variabel_width_kde(image, perspecuity)
