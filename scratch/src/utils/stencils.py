import numba as nb

def get_stencil(N):
    """NxN stencil of ones."""
    def summator(x):
        cmul = 0
        for i in range(-N//2, N//2):
            for j in range(-N//2, N//2):
                cmul += x[i, j]
        return cmul

    return nb.stencil(neighborhood=((-N//2, N//2), (-N//2, N//2)))(summator)
