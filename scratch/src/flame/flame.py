from math import sin, cos, pi, sqrt, exp, cosh, sinh, log

import numpy as np
import numba as nb

from numba.types import float64 as nbfloat64
from numba.types import int64 as nbint64
from numba.types import void as nbvoid


NB_KWARGS = {
    "cache": True,
    "nopython": True,
    "nogil": True,
    "fastmath": True
}


def make_histogram(
        affine_colour_transform,
        frequency_histogram,
        colour_histogram,
        num_points,
        maxiter,
        variation=None,
        post_transform=None,
        cutoff=1
):
    """Make the histogram over frequencies and colors. 

    Args:
        affine_colour_transform: Callable 
    """

    if variation is None:
        def variation(x):
            return x

    if post_transform is None:
        def post_transform(x):
            return x

    # Compile colour transform
    inner_affine_colour_transform = nb.jit(nbvoid(nbfloat64[:], nbfloat64[:]), **NB_KWARGS)(
        affine_colour_transform
    )

    # Compile variation and post_transform
    inner_variation = nb.jit(nbvoid(nbfloat64[:]), **NB_KWARGS)(variation)
    inner_post_transform = nb.jit(nbvoid(nbfloat64[:]), **NB_KWARGS)(post_transform)

    @nb.jit(nbvoid(nbfloat64[:, :], nbfloat64[:, :, :]), **NB_KWARGS)
    def inner_loop(fhist, chist):
        # Define histogram edges
        x_edge = np.linspace(0, 1, frequency_histogram.shape[0])
        y_edge = np.linspace(0, 1, frequency_histogram.shape[1])

        colour_array = np.zeros(3)      # rgb
        xy_array_pair = np.zeros(2)     # A point in the x, y plane

        for n in range(num_points):
            xy_array[:] = np.random.uniform(0, 1, size=2)

            for i in range(maxiter):
                if i < cutoff:      # break iteration if under cutoff
                    continue

                inner_affine_colour_transform(xy_array, colour_array)
                inner_variation(xy_array)
                inner_post_transform(xy_array)

                # Compute correct indices in the histogram
                xidx = np.searchsorted(x_edge, xy_array_pair[0])
                yidx = np.searchsorted(y_edge, xy_array_pair[1])

                frequency_histogram[xidx, yidx] += 1
                colour_histogram[xidx, yidx] += colour_array
                colour_histogram[xidx, yidx] /= 2

    # Make the histograms
    inner_loop(fhist, chist)
    chist /= 255        # Rescale colors to [0, 1]. This works better with the KDE.


@nb.jit(nbvoid(nbfloat64[:, :], nbfloat64[:, :, :]), **NB_KWARGS)
def gamma(frequency_histogram, colour_histogram):
    """Gamma correction as per the wikipedia flame page."""
    max_frequency = frequency_histogram.max()
    for i in range(frequency_histogram.shape[0]):
        for j in range(frequency_histogram.shape[1]):
            if frequency_histogram[i, j] > 0:
                colour_histogram[i, j] *= (
                    np.log(frequency_histogram[i, j])/np.log(max_frequency)
                )**(1/2.2)
