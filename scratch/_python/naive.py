import numpy as np

from plotting import (
    plot,
)


def inner_loop(z, maxiter):
    c = z
    for n in range(maxiter):
        if abs(z) > 2:
            return n
        z = z*z + c
    return maxiter


def naive(maxiter, xmin, xmax, ymin, ymax, height, width):
    r1 = np.linspace(xmin, xmax, width)
    r2 = np.linspace(ymin, ymax, height)
    n3 = np.empty((width, height))
    for i in range(width):
        for j in range(height):
            n3[i, j] = inner_loop(r1[i] + 1j*r2[j], maxiter)
    return (r1, r2, n3)


if __name__ == "__main__":
    args = (naive, -2.0, 0.5, -1.25, 1.25)
    plot(*args)
