"""Script form visualising the mandelbott set."""

import numpy as np
import numba as nb

from numba.types import void as nbvoid
from numba.types import float64 as nbfloat64
from numba.types import int64 as nbint64

from timeit import default_timer as clock
from termcolor import colored

from plotting import (
    plot,
)


nb_kwargs = {
    "cache": True,
    "nopython": True,
    "nogil": True,
    "fastmath": True
}


def timeit(method):

    def timed(*args, **kw):
        tic = clock()
        result = method(*args, **kw)
        toc = clock()

        print(colored(toc - tic, "red"))
        return result

    return timed


@nb.jit(nbfloat64(nbfloat64, nbfloat64, nbfloat64, nbfloat64, nbfloat64), **nb_kwargs)
def inner_loop(creal, cimag, maxiter, horizon, log_horizon):
    real = creal
    imag = cimag
    for n in range(maxiter):
        real2 = real*real
        imag2 = imag*imag
        if real2 + imag2 > horizon:
            return n - np.log(np.log(np.abs(real + 1j*imag)))/np.log(2) + log_horizon
        imag = 2*real*imag + cimag
        real = real2 - imag2 + creal
    return 0


@timeit
@nb.jit(nb.float64[:, :](nb.int64, nb.float64, nb.float64, nb.float64, nb.float64, nb.int64, nb.int64), **nb_kwargs)
def mandel(maxiter, xmin, xmax, ymin, ymax, height, width):
    horizon = 2.0 ** 40
    log_horizon = np.log(np.log(horizon))/np.log(2)

    r1 = np.linspace(xmin, xmax, width)
    r2 = np.linspace(ymin, ymax, height)
    n3 = np.zeros((width, height))

    for i in range(width):
        for j in range(height):
            n3[i, j] = inner_loop(r1[i], r2[j], maxiter, horizon, log_horizon)
    return n3


if __name__ == "__main__":
    # width = 1. / 2**15
    width = 1. / 2**34
    print("{:.16f}".format(width))

    xc = -0.743643887037151
    yc = 0.131825904205330

    # width = 0.000000000051299
    xmin = xc - width/2
    xmax = xc + width/2

    ymin = yc - width/2
    ymax = yc + width/2

    # args = (mandel, -0.75, -0.745, 0.05, 0.055)

    args = (mandel, xmin, xmax, ymin, ymax)
    # kwargs = {"maxiter": 4096*4, "cmap": "gist_heat", "gamma": 0.9}
    kwargs = {"maxiter": 4096*4, "cmap": "gist_heat", "gamma": 0.9}
    plot(*args, **kwargs)
