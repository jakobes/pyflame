import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt
from PIL import Image

import numba as nb


def plot(mandel, xmin, xmax, ymin, ymax, width=30, height=30,\
                     maxiter=1024, cmap='viridis', gamma=0.3, dpi=226):
    img_width = dpi*width
    img_height = dpi*height
    img = mandel(maxiter, xmin, xmax, ymin, ymax, img_width, img_height)
    print(img.mean())
    np.save("master.npy", img)

    fig, ax = plt.subplots(1)
    light = mpl.colors.LightSource(azdeg=314, altdeg=10)
    # img = light.shade(
    #     img,
    #     cmap=plt.cm.gist_heat,
    #     vert_exag=1.5,
    #     # norm=mpl.colors.PowerNorm(gamma)
    #     norm=mpl.colors.LogNorm(),
    #     blend_mode='hsv'
    # )
    # TODO: Need to set 
    # ax.imshow(img.T, extent=[xmin, xmax, ymin, ymax], origin="lower", cmap=cmap)
    ax.imshow(img.T, origin="lower", cmap=cmap)
    ax.set_xticks([])
    ax.set_yticks([])
    fig.savefig("bar.png")

    # norm = mpl.colors.PowerNorm(gamma)
    """
    norm = mpl.colors.LogNorm()

    mapper = plt.cm.ScalarMappable(norm=norm, cmap=cmap)
    im = Image.fromarray(np.uint8(mapper.to_rgba(img.T)*255))
    im.save("julia.png")
    """
