import numpy as np
from numba import vectorize, complex64, boolean, jit


from plotting import (
    plot,
)

@vectorize([boolean(complex64)])
def f(z):
    return (z.real*z.real + z.imag*z.imag) < 4.0


@vectorize([complex64(complex64, complex64)])
def g(z, c):
    return z*z + c

@jit
def inner_loop(c, maxiter):
    output = np.zeros(c.shape, np.int)
    z = np.empty(c.shape, np.complex64)

    for it in range(maxiter):
        notdone = f(z)
        output[notdone] = it
        z[notdone] = g(z[notdone], c[notdone])
    output[output == maxiter - 1] = 0
    return output


def numpyLoops(maxiter, xmin, xmax, ymin, ymax, height, width):
    r1 = np.linspace(xmin, xmax, width)
    r2 = np.linspace(ymin, ymax, height)
    n3 = np.empty((width, height))

    c = (r1 + r2[:, None]*1.j).astype(np.complex64)
    n3 = inner_loop(c, maxiter)
    return r1, r2, n3.T


if __name__ == "__main__":
    args = (numpyLoops, -2.0, 0.5, -1.25, 1.25)
    plot(*args)
