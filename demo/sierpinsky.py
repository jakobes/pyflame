"""Plot the Sierpinsky triangle."""

import numpy as np
import matplotlib.pyplot as plt

from pyflame import (
    make_histogram,
    gamma,
)
from PIL import Image


def affine_colour_transform(xy_array, colour_array):
    """Affine transform. This is jitted inside `make_histogram`."""
    x, y = xy_array
    i = np.random.randint(3)
    colour_space = np.array([
        (155, 0, 0),
        (0, 155, 0),
        (0, 0, 155),
    ])
    if i == 0:
        x, y = (1 + x)/2, (1 + y)/2
    elif i == 1:
        x, y = (1 + x)/2, y/2
    elif i == 2:
        x, y = x/2, (1 + y)/2

    xy_array[:] = x, y
    colour_array[:] = colour_space[i].astype(np.float64)


if __name__ == "__main__":
    import time
    N = 200
    fhist = np.zeros(shape=(N, N))
    chist = np.zeros(shape=(N, N, 3))

    tick = time.clock()
    make_histogram(
        affine_colour_transform,
        fhist,
        chist,
        num_points=10000,
        maxiter=30,
        cutoff=15
    )
    tock = time.clock()
    print("histogram finished: ", tock - tick)

    tick = time.clock()
    # fhist = kde(fhist, 20)
    tock = time.clock()
    print("compute kde: ", tock - tick)

    print("chist extreme", chist.max(), chist.min())
    tick = time.clock()
    # for c in range(3):
    #     chist[:, :, c] = variable_kde(chist[:, :, c], 10, 1)

    tock = time.clock()
    print("colour kde: ", tock - tick)
    print("chist extreme", chist.max(), chist.min())

    tick = time.clock()
    gamma(fhist, chist)
    tock = time.clock()
    print("gamma finished: ", tock - tick)
    # np.save("chist.npy", chist)

    chist *= 255
    img = Image.fromarray(chist.astype(np.uint8))
    img.save("bar.png")
