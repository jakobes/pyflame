from pathlib import Path

from pymandelbrot import (
    compute_mandelbrot,
    plot_image,
)


def get_bounds(xc, yc, width):
    xmin = xc - width/2
    xmax = xc + width/2

    ymin = yc - width/2
    ymax = yc + width/2
    return xmin, xmax, ymin, ymax


if __name__ == "__main__":
    width = 1./2**34
    xc = -0.743643887037151
    yc = 0.131825904205330
    xmin, xmax, ymin, ymax = get_bounds(xc, yc, width)

    # dpi = 226
    # dpi = 226
    # width = 30
    # height = 30

    img_width = 1920*10
    img_height = 1080*10
    # img_width = 1000
    # img_height = 1000

    # maxiter = 4096*8
    maxiter = 4096*8
    cmap = "copper"

    cmaps = {}
    cmaps['Perceptually-Uniform-Sequential'] = [
        'viridis', 'plasma', 'inferno', 'magma', 'cividis'
    ]

    cmaps['Sequential'] = [
        'Greys', 'Purples', 'Blues', 'Greens', 'Oranges', 'Reds',
        'YlOrBr', 'YlOrRd', 'OrRd', 'PuRd', 'RdPu', 'BuPu',
        'GnBu', 'PuBu', 'YlGnBu', 'PuBuGn', 'BuGn', 'YlGn'
    ]
    cmaps['Sequential2'] = [
        'binary', 'gist_yarg', 'gist_gray', 'gray', 'bone', 'pink',
        'spring', 'summer', 'autumn', 'winter', 'cool', 'Wistia',
        'hot', 'afmhot', 'gist_heat', 'copper'
    ]

    cmaps['Diverging'] = [
        'PiYG', 'PRGn', 'BrBG', 'PuOr', 'RdGy', 'RdBu',
        'RdYlBu', 'RdYlGn', 'Spectral', 'coolwarm', 'bwr', 'seismic'
    ]

    cmaps['Cyclic'] = ['twilight', 'twilight_shifted', 'hsv']

    cmaps['Qualitative'] = [
        'Pastel1', 'Pastel2', 'Paired', 'Accent',
        'Dark2', 'Set1', 'Set2', 'Set3',
        'tab10', 'tab20', 'tab20b', 'tab20c'
    ]

    # scheme_type = "Perceptually-Uniform-Sequential"
    # scheme_type = "Sequential"
    scheme_type = "Sequential2"
    # scheme_type = "Diverging"
    # scheme_type = "Cyclic"
    figdir = Path(scheme_type)
    figdir.mkdir(exist_ok=True)
    for cmap in cmaps[scheme_type]:
        print(cmap)
        img = compute_mandelbrot(maxiter, xmin, xmax, ymin, ymax, img_height, img_width)
        plot_image(img, cmap=cmap, light=False, filename=figdir / f"{cmap}.png")
