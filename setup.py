from setuptools import setup, find_packages

setup(
    name="pyflame",
    author="Jakob E. Schreiner",
    author_email="jakobes@protonmail.com",
    packages=find_packages("pyflame"),
    package_dir={"": "pyflame"},
    install_requires=[
        "numba",
        "numpy",
        "matplotlib",
        "Pillow"
    ]
)
