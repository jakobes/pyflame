"""Compute the Mandelbrot set. Implementation taken from 'IT best secret is optimisation'."""
import numpy as np
import numba as nb

# Alias specific numba types
from numba.types import void as nbvoid
from numba.types import float64 as nbfloat64
from numba.types import int64 as nbint64

import time


NB_KWARGS = {
    "cache": True,
    "nopython": True,
    "nogil": True,
    "fastmath": True
}


@nb.jit(nbfloat64(nbfloat64, nbfloat64, nbfloat64, nbfloat64, nbfloat64), **NB_KWARGS)
def _inner_mandelbrot_loop(*, c_real, c_imag, maxiter, horizon, log_horizon):
    """Fixed point iteration of the mandelbrot set."""
    real = c_real        # real part of z
    imag = c_imag        # complex partof z

    for n in range(maxiter):
        real2 = real*real
        imag2 = imag*imag

        if real2 + imag2 > horizon:
            return n - np.log(np.log(np.abs(real + 1j*imag)))/np.log(2) + log_horizon
        imag = 2*real*imag + c_imag
        real = real2 - imag2 + c_real
    return 0


@nb.jit(nb.float64[:, :](
    nb.int64, nb.float64, nb.float64, nb.float64, nb.float64, nb.int64, nb.int64
), **NB_KWARGS)
def compute_mandelbrot(maxiter, xmin, xmax, ymin, ymax, height, width):
    horizon = 2.0 ** 40
    log_horizon = np.log(np.log(horizon))/np.log(2)

    r1 = np.linspace(xmin, xmax, width)
    r2 = np.linspace(ymin, ymax, height)
    n3 = np.zeros((width, height))      # The image

    for i in range(width):
        for j in range(height):
            n3[i, j] = _inner_mandelbrot_loop(r1[i], r2[j], maxiter, horizon, log_horizon)
    return n3


if __name__ == "__main__":
    width = 1. / 2**34

    xc = -0.743643887037151
    yc = 0.131825904205330

    xmin = xc - width/2
    xmax = xc + width/2

    ymin = yc - width/2
    ymax = yc + width/2

    # dpi = 226
    dpi = 31
    width = 30
    height = 30
    img_width = dpi*width
    img_height = dpi*height

    # maxiter = 4096*4
    maxiter = 4096*4

    img = compute_mandelbrot(maxiter, xmin, xmax, ymin, ymax, img_width, img_height)

    from plot_mandelbrot import plot_image

    plot_image(img, cmap="gist_heat", light=False, filename="foo.png")
