"""Compute and plot the mandelbrot set."""

from .compute_mandelbrot import compute_mandelbrot
from .plot_mandelbrot import plot_image
