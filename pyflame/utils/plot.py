"""Plot the Mandelbrot set."""

import numpy as np

import matplotlib as mpl
import matplotlib.pyplot as plt

from pathlib import Path


def plot_grayscale(image_array: np.ndarray, colormap: str, figname: Path) -> None:
    fig = plt.figure()
    ax = fig.add_saxes([0.0, 0.0, 1.0, 1.0], frameon=False, aspect=1)
    norm = mpl.colors.LogNorm()     # vmin, vmax, clip

    mapper = plt.cm.ScalarMappable(norm=norm, cmap=colormap)
    color_image = mapper.to_rgba(image_array.T)

    ax.imshow(color_image)
    ax.set_xticks([])
    ax.set_yticks([])
    plt.savefig(figname)
