import time


def timeit(method):
    """Function decorator to time the execution time of functions."""

    def timed(*args, **kw):
        tic = time.clock()
        result = method(*args, **kw)
        toc = time.clock()

        print(f"Time: {toc - tic}")
        return result

    return timed
