"""A  collectionof utility functions."""


from .utils import timeit
from .plot import plot_grayscale
